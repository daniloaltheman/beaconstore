var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var iBeaconSchema = new Schema({
	name: {type: String, required: true, index: {unique: true, dropDups: true}},
	price: {type: String, required: false},
	uuid: {type: String, required: true},
	major: {type: String, required: true},
	minor: {type: String, required: true},
	section: {type: String, required: false}
});
module.exports = Mongoose.model('ibeacons', iBeaconSchema);