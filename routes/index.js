var express = require('express');
var router = express.Router();
var iBeacon = require('../models/ibeacon.js');
var app = require('../app.js');
var uploader = require('blueimp-file-upload-expressjs')(options);
// config the uploader
var options = {
    tmpDir: __dirname + '/../public/uploaded/tmp',
    publicDir: __dirname + '/../public/uploaded',
    uploadDir: __dirname + '/../public/uploaded/files',
    uploadUrl: '/uploaded/files/',
    maxPostSize: 11000000000, // 11 GB
    minFileSize: 1,
    maxFileSize: 10000000000, // 10 GB
    acceptFileTypes: /.+/i,
    // Files not matched by this regular expression force a download dialog,
    // to prevent executing any scripts in the context of the service domain:
    inlineFileTypes: /\.(gif|jpe?g|png)$/i,
    imageTypes: /\.(gif|jpe?g|png)$/i,
    imageVersions: {
        maxWidth: 80,
        maxHeight: 80
    },
    accessControl: {
        allowOrigin: '*',
        allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
        allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
    },
    storage : {
        type : 'local'
    },
    nodeStatic: {
        cache: 3600 // seconds to cache served files
    }
};



/* GET  - Render home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// curl localhost:3000 --data "name=Vixi Nossa&uuid=1238182381823812&major=0&minor=1&description=&price=200.00"
router.post('/', function(req, res, next) {

	var ibeacon = new iBeacon(req.body);
	ibeacon.save(function(err, result) {
		if(err) {
			console.log(err);
			res.status(400).json({response: err.message});
		}
		else {
			res.status(200).json({response: "Successful!"});
		}
	});
});

router.post('/upload', function(req, res, next) {
	uploader.post(req, res, function(err, obj) {
	    res.send(JSON.stringify(obj));
	});
});

router.get('/upload', function(req, res) {
    uploader.get(req, res, function(err, obj) {
        res.send(JSON.stringify(obj));
    });
});

router.delete('/uploaded/files/:name', function(req, res) {
    uploader.delete(req, res, function(err, obj) {
        res.send(JSON.stringify(obj));
    });
});

// curl localhost:3000/find/Bnlqb
router.get('/find/:uuid', function(req, res, next) {
  iBeacon.find({uuid: req.params.uuid}, function(err, result){
    if(err) res.status(400).json({response: err});
    else res.json({response: result});
  });
});

// curl localhost:3000/list
router.get('/list', function(req, res){
  iBeacon.find({}, function(err, result){
    if(err) res.json(err);
    else res.status(200).json({response: result});
    // else res.render('Customers/Customer_list', {Customers: result});
  });
});

router.delete('/:id', function(req, res) {
	iBeacon.findOneAndRemove({_id: req.params.id}, function(err, result) {
		if(result == null) res.status(400).json({response: "iBeacon not found!", error: true});
		else res.json({response:"iBeacon removed!", ibeacon_id: result._id, error: false});
	});
});

module.exports = router;
